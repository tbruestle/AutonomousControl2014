function [ newx, newy ] = pso( px, py, pbestx, pbesty, gbestx, gbesty )
%PSO Summary of this function goes here
%   Detailed explanation goes here

newx = px + (pbestx - px)*0.1 + (gbestx - px)*0.1;
newy = py + (pbesty - py)*0.1 + (gbesty - py)*0.1;

end

