
%sphere
tic;
A = [(randi(40,10,2) - 20) zeros(10,4)];
gbest = 1;
for m = 1:10
    A(m,3) = sphere( [A(m,1), A(m,2)] );
    B =fmincon('sphere',[A(m,1), A(m,2)],[],[],[],[],[],[],'noconstraints');
    A(m,4) = B(1);
    A(m,5) = B(2);
    A(m,6) = sphere( [A(m,4), A(m,5)] );
end
sphereXYCost = A(gbest, 4:6);
sphereTime = toc;




%matyas
tic;
A = [(randi(40,10,2) - 20) zeros(10,4)];
gbest = 1;
for m = 1:10
    A(m,3) = matyas( [A(m,1), A(m,2)] );
    B =fmincon('matyas',[A(m,1), A(m,2)],[],[],[],[],[],[],'noconstraints');
    A(m,4) = B(1);
    A(m,5) = B(2);
    A(m,6) = matyas( [A(m,4), A(m,5)] );
end
matyasXYCost = A(gbest, 4:6);
matyasTime = toc;



%ackleys
tic;
A = [(randi(40,10,2) - 20) zeros(10,4)];
gbest = 1;
for m = 1:10
    A(m,3) = ackleys( [A(m,1), A(m,2)] );
    B =fmincon('ackleys',[A(m,1), A(m,2)],[],[],[],[],[],[],'noconstraints');
    A(m,4) = B(1);
    A(m,5) = B(2);
    A(m,6) = ackleys( [A(m,4), A(m,5)] );
end
ackleysXYCost = A(gbest, 4:6);
ackleysTime = toc;
clc
sphereXYCost
matyasXYCost
ackleysXYCost
sphereTime
matyasTime
ackleysTime