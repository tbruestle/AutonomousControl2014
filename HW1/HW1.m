%For simplicity, execute by a number of iterations
iterations = 100

%sphere
tic;
A = [(randi(40,10,2) - 20) zeros(10,4)];
gbest = 1;
for m = 1:10
    A(m,3) = sphere( [A(m,1), A(m,2)] );
    A(m,4) = A(m,1);
    A(m,5) = A(m,2);
    A(m,6) = A(m,3);
    if  A(m,6) < A(gbest, 6)
        gbest = m;
    end
end
for n = 1:iterations
    for m = 1:10
        [ A(m,1), A(m,2) ] = ...
            pso( A(m,1), A(m,2), A(m,4), A(m,5), A(gbest,4), A(gbest,5) );
        A(m,3) = sphere( [A(m,1), A(m,2)] );
        if  A(m,3) < A(m, 6)
            A(m,4) = A(m,1);
            A(m,5) = A(m,2);
            A(m,6) = A(m,3);
        end
        if  A(m,6) < A(gbest, 6)
            gbest = m;
        end
    end
end
sphereXYCost = A(gbest, 4:6);
sphereTime = toc;


%matyas
tic;
A = [(randi(40,10,2) - 20) zeros(10,4)];
gbest = 1;
for m = 1:10
    A(m,3) = matyas( [A(m,1), A(m,2)] );
    A(m,4) = A(m,1);
    A(m,5) = A(m,2);
    A(m,6) = A(m,3);
    if  A(m,6) < A(gbest, 6)
        gbest = m;
    end
end
for n = 1:iterations
    for m = 1:10
        [ A(m,1), A(m,2) ] = ...
            pso( A(m,1), A(m,2), A(m,4), A(m,5), A(gbest,4), A(gbest,5) );
        A(m,3) = matyas( [A(m,1), A(m,2)] );
        if  A(m,3) < A(m, 6)
            A(m,4) = A(m,1);
            A(m,5) = A(m,2);
            A(m,6) = A(m,3);
        end
        if  A(m,6) < A(gbest, 6)
            gbest = m;
        end
    end
end
matyasXYCost = A(gbest, 4:6);
matyasTime = toc;



%ackleys
tic;
A = [(randi(40,10,2) - 20) zeros(10,4)];
gbest = 1;
for m = 1:10
    A(m,3) = ackleys( [A(m,1), A(m,2)] );
    A(m,4) = A(m,1);
    A(m,5) = A(m,2);
    A(m,6) = A(m,3);
    if  A(m,6) < A(gbest, 6)
        gbest = m;
    end
end
for n = 1:iterations
    for m = 1:10
        [ A(m,1), A(m,2) ] = ...
            pso( A(m,1), A(m,2), A(m,4), A(m,5), A(gbest,4), A(gbest,5) );
        A(m,3) = ackleys( [A(m,1), A(m,2)] );
        if  A(m,3) < A(m, 6)
            A(m,4) = A(m,1);
            A(m,5) = A(m,2);
            A(m,6) = A(m,3);
        end
        if  A(m,6) < A(gbest, 6)
            gbest = m;
        end
    end
end
ackleysXYCost = A(gbest, 4:6);
ackleysTime = toc;
clc
sphereXYCost
matyasXYCost
ackleysXYCost
sphereTime
matyasTime
ackleysTime